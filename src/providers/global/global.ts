import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Subject } from 'rxjs';
import io from 'socket.io-client';

import { Player } from '../../interfaces/player';
import { EVENTS } from '../../events';

@Injectable()
export class GlobalProvider {
    public stateEmitter: Subject<boolean> = new Subject();

    private _gameState: boolean = false;
    private _playerCount: number = 0;
    private _winner: Player | undefined;
    private _me: Player;
    private _answer: number;
    private socket: SocketIOClient.Socket;

    public in: boolean = false;
    public fakeAdmin: SocketIOClient.Socket;

    public get gameState(): boolean {
        return this._gameState;
    }

    public get winner(): Player {
        return this._winner;
    }

    public get answer(): number {
        return this._answer;
    }

    public get me(): Player {
        return this._me;
    }

    public get playerCount(): number {
        return this._playerCount;
    }

    constructor(private alert: AlertController) {

        this.socket = io('https://www.cin.party/player', { path: '/demo_socket/' });
        // this.socket = io('localhost:4000/player', { path: '/' });
        this.socket.connect();

        // this.fakeAdmin = io('localhost:4000/admin', { path: '/' });
        this.fakeAdmin = io('https://www.cin.party/admin', { path: '/demo_socket/'});
        this.fakeAdmin.connect();

        this.socket.on(EVENTS.GAME_STATE_CHANGED, (state, winner, answer) => { this.setState(state, winner, answer); });
        this.socket.on(EVENTS.PLAYER_COUNT_CHANGED, (count: number) => { this.setCount(count); });

        this.socket.on(EVENTS.CONNECT, (error: Error) => {
            this.resetPlayer();
        });


        this.socket.on(EVENTS.ERROR, (error: Error) => {
            let alert = this.alert.create({
                title: error.name,
                subTitle: error.message,
                buttons: ['OK']
            });
            alert.present();
        });

        this.socket.on(EVENTS.PLAYER_STATE, (playerState: Player) => {
            this.me.username = playerState.username;
            this.me.chance = playerState.chance;
        });

        this.socket.on(EVENTS.DISCONNECT, (reason: string) => {
            this.in = false;
            this.resetPlayer();
        })
    }

    public join(username: string): Promise<boolean> {
        return new Promise((resolve, reject) => {
            this.socket.emit(EVENTS.JOIN, username, (result: boolean) => {
                resolve(result);
            });
        }).then((res: boolean) => {
            this.in = res;
            return res;
        });
    }

    public guess(num: number): Promise<string> {
        return new Promise((resolve, reject) => {
            this.socket.emit(EVENTS.GUESS, num, (result: string) => {
                resolve(result);
            });
        });
    }

    private setState(state: boolean, winner: Player, answer: number) {
        this.stateEmitter.next(state);
        this._winner = winner;
        this._answer = answer;
        this._gameState = state;

        if (winner && answer) {
            if (winner.id !== this.me.id) {
                let alert = this.alert.create({
                    title: 'You lost!',
                    subTitle: `Player with id ${winner.id} won! The answer was ${answer}.`,
                    buttons: ['OK :(']
                });
                alert.present();
            } else {
                let alert = this.alert.create({
                    title: 'You won!',
                    subTitle: `Congratulations, ${winner.username}`,
                    buttons: ['Alright']
                });
                alert.present();
            }
        }
    }

    private resetPlayer() {
        this._me = {
            username: '',
            id: this.socket.id,
            chance: 0
        };
    }

    private setCount(count: number) {
        this._playerCount = count;
    }

}
