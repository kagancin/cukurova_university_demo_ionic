export const EVENTS = {
    START: 'start',
    END: 'end',
    JOIN: 'join',
    GUESS: 'guess',
    CONNECTION: 'connection',
    CONNECT: 'connect',
    DISCONNECT: 'disconnect',
    ERROR: 'error',
    GAME_STATE_CHANGED: 'game_state',
    PLAYER_COUNT_CHANGED: 'player_count',
	PLAYER_STATE: 'player_state'
};
