export interface Player {
    username: string;
    id: string;
    chance: number;
}
