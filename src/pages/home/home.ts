import { Component, OnInit, OnDestroy } from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Subscription } from 'rxjs';
import { GlobalProvider } from '../../providers/global/global';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage implements OnInit, OnDestroy {
    gameState: boolean;
    stateSubscription: Subscription;
    magic: number;
    username: string;
    constructor(private alert: AlertController, public global: GlobalProvider) {
        this.gameState = this.global.gameState;
    }

    ngOnInit() {
        this.stateSubscription = this.global.stateEmitter.subscribe((state: boolean) => {
            this.gameState = state;
        });
    }

    join() {
        this.global.join(this.username).then((res: boolean) =>  {
            if (res === true) {
                let alert = this.alert.create({
                    title: `Welcome ${this.username}`,
                    subTitle: `Let's start to play`,
                    buttons: ['OK']
                });
                alert.present();
            } else {
                let alert = this.alert.create({
                    title: `Sorry ${this.username}`,
                    subTitle: 'This username is already taken',
                    buttons: ['OK']
                });
                alert.present();
            }
        });
    }

    guess() {
        this.global.guess(this.magic).then((res: string) => {
            this.magic = null;

            if (typeof res === 'string') {
                let alert = this.alert.create({
                    title: 'Not good!',
                    subTitle: res,
                    buttons: ['OK']
                });
                alert.present();
            }

        })
    }

    ngOnDestroy() {
        this.stateSubscription.unsubscribe();
    }

}
